import unittest
from django.test import TestCase, Client 
from django.contrib.auth.models import User

class LoginTest(TestCase):
    #Se inicializa el test, en donde se crea una instancia de Client para simular peticiones HTTP
     def setUp(self):
         self.client = Client()
         self.user = User.objects.create(username='user')
         self.user.set_password('usergcctp2')
         self.user.save()

     def test_login(self):
         #CON EL USER Y EL PASS VALIDO
         #Con la instancia de cliente llamo a mi url de login
         response = self.client.get('/accounts/login/')
         #Compruebo si la url existe
         self.assertEquals(response.status_code, 200)
         #Hago la llamada a login desde la instancia de client
         response_login = self.client.login(username='user', password="usergcctp2")
         #Compruebo si el login esta correcto
         self.assertEquals(response_login, True)
     def test_login2(self):
         #CON EL USER INCORRECTO Y EL PASS VALIDO
         #Con la instancia de cliente llamo a mi url de login
         response = self.client.get('/accounts/login/')
         #Compruebo si la url existe
         self.assertEquals(response.status_code, 200)
         #Hago la llamada a login desde la instancia de client
         response_login = self.client.login(username='user', password="usergcctp2")
         #Compruebo si el login esta correcto
         self.assertEquals(response_login, True)
     def test_login3(self):
         #CON EL USER CORRECTO Y EL PASS NO VALIDO
         #Con la instancia de cliente llamo a mi url de login
         response = self.client.get('/accounts/login/')
         #Compruebo si la url existe
         self.assertEquals(response.status_code, 200)
         #Hago la llamada a login desde la instancia de client
         response_login = self.client.login(username='user', password="usergcctp2")
         #Compruebo si el login esta correcto
         self.assertEquals(response_login, True)