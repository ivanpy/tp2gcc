# App de autenticación en Django



## Para correr localmente:

Se debe tener instalado Python Heroku Toolbelt y Postgres.

#PASOS:
```sh
$ git clone https://gitlab.com/ivanpy/tp2gcc.git
$ cd gcctp2
$ pip install -r requirements.txt
$ createdb gcctp2
$ foreman run python manage.py migrate
$ python manage.py collectstatic
$ foreman start web
```

La app corre en [localhost:5100](http://localhost:5100/).

## Para el deploy en Heroku:

```sh
$ heroku create
$ git push heroku master
$ heroku open
```